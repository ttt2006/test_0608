import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
// 导入登录页面
import login from '@/page/main/login.vue'
// 导入以下页面
import schoolList from '@/page/school/list.vue'
import schoolAdd from '@/page/school/add.vue'
import schoolEdit from '@/page/school/edit.vue'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/schoolList',
      name: 'schoolList',
      component: schoolList
    },
    {
      path: '/schoolAdd',
      name: 'schoolAdd',
      component: schoolAdd
    },
    {
      path: '/schoolEdit',
      name: 'schoolEdit',
      component: schoolEdit
    }
  ]
})
