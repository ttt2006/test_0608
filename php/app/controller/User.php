<?php

declare(strict_types=1);

namespace app\controller;

use app\model\User as ModelUser;
use app\model\UserInfo;
use think\facade\Validate;
use think\Request;

class User
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }

    /**
     * 登录
     */

    public function login(Request $request, ModelUser $user)
    {
        // 接收数据
        $data['name'] = $request->param('name', '');
        $data['pwd'] = $request->param('pwd', '');
        // 验证数据
        $rule = [
            'name' => 'require|max:30|min:6',
            'pwd'  => 'require|max:18|min:6'
        ];
        $message = [
            'name.require' => '用户名不能为空',
            'name.max'     => '用户名最多不能超过30位',
            'name.min'     => '用户名最少不能小于6位',
            'pwd.require'  => '密码不能为空',
            'pwd.max'      => '密码最多不能超过18位',
            'pwd.min'      => '密码最少不能小于6位'
        ];
        $validate = Validate::rule($rule)->message($message);
        if (!$validate->check($data)) {
            return json(['code' => 1, 'msg' => $validate->getError(), 'res' => null]);
        }
        // 调用模型
        $res = $user->selOne($data);
        if ($res) {
            return json(['code' => 0, 'msg' => '登录成功', 'res' => $res]);
        } else {
            return json(['code' => 1, 'msg' => '用户名或密码错误', 'res' => $res]);
        }
    }

    /**
     * applets login
     */

    public function loginApplets(Request $request, UserInfo $userInfo)
    {
        // 获取数据
        $data['u_openid'] = $request->param('openid', '');
        // 验证数据
        $rule = [
            'u_openid' => 'require|max:200|min:10'
        ];
        $message = [
            'u_openid.require' => 'openid 不能为空',
            'u_openid.max'     => 'openid 格式错误',
            'u_openid.min'     => 'openid 格式错误'
        ];
        $validate = Validate::rule($rule)->message($message);
        if (!$validate->check($data)) {
            return json(['code' => 1, 'msg' => $validate->getError(), 'res' => null]);
        }
        // 根据 openid 判断是否存在
        $where['u_openid'] = $data['u_openid'];
        $user = $userInfo->selOne($where);
        if (!$user) {
            return json(['code' => 1, 'msg' => '还没授权登录、请先授权然后登录', 'res' => $user]);
        }
        return json(['code' => 0, 'msg' => '已授权获取到用户的数据', 'res' => $user]);
    }

    /**
     * 操作用户信息
     * 存在、更新
     * 不存在、添加
     */

    public function appletsUserInfo(Request $request, UserInfo $userInfo)
    {
        // 获取数据
        $data['u_openid'] = $request->param('openid', '');
        $data['u_avatarUrl'] = $request->param('avatarUrl', '');
        $data['u_city'] = $request->param('city', '');
        $data['u_country'] = $request->param('country', '');
        $data['u_gender'] = $request->param('gender', '');
        $data['u_language'] = $request->param('language', '');
        $data['u_nickName'] = $request->param('nickName', '');
        // 验证数据
        $rule = [
            'u_openid' => 'require|max:200|min:10',
            'u_avatarUrl' => 'require',
            'u_nickName' => 'require'
        ];
        $message = [
            'u_openid.require'      => 'openid 不能为空',
            'u_openid.max'          => 'openid 格式错误',
            'u_openid.min'          => 'openid 格式错误',
            'u_avatarUrl.require'   => '用户头像 不能为空',
            'u_nickName.max'        => '用户名 格式错误',
        ];
        $validate = Validate::rule($rule)->message($message);
        if (!$validate->check($data)) {
            return json(['code' => 1, 'msg' => $validate->getError(), 'res' => null]);
        }

        // 根据 openid 判断是否存在
        $where['u_openid'] = $data['u_openid'];
        $user = $userInfo->selOne($where);

        // 存在、执行修改
        if ($user) {
            $user_res = $userInfo->updOne($where, $data);
            $res = [];
            $res['u_id'] = $user['u_id'];
            $res['u_regtime'] = $user['u_regtime'];
        }

        // 不存在、执行添加
        if (empty($user)) {
            $res = [];
            $res = $data;
            $res['u_regtime'] = time();
            $res['u_id'] = $userInfo->addOne($res);
        }

        // 判断是否添加成功
        if (empty($res['u_id'])) {
            return json(['code' => 1, 'msg' => '注册失败,返回重试', 'res' => null]);
        }
        return json(['code' => 0, 'msg' => 'ok', 'res' => $res]);
    }
}
