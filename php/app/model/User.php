<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class User extends Model
{
    // 表名
    protected $table = 'user';
    // 主键ID
    protected $pk = 'id';
    // 设置字段信息
    protected $schema = [
        'id'    => 'int',
        'name'  => 'string',
        'pwd'   => 'string'
    ];

    /**
     * 获取一条数据
     */

    public function selOne($where)
    {
        return self::where( $where )->find();
    }
}
