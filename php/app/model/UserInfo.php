<?php

declare(strict_types=1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class UserInfo extends Model
{
    // 表名
    protected $table = 'user_info';
    // 主键ID
    protected $pk = 'u_id';
    // 设置字段信息
    protected $schema = [
        'u_id' => 'int',
        'u_openid' => 'string',
        'u_avatarUrl' => 'string',
        'u_city' => 'string',
        'u_country' => 'string',
        'u_gender' => 'string',
        'u_language' => 'string',
        'u_nickName' => 'string',
        'u_regtime' => 'string',
    ];

    /**
     * 添加一条数据
     */

    public function addOne($data)
    {
        return self::insertGetId($data);
    }

    /**
     * 删除一条数据
     */

    public function delOne($where)
    {
        return self::where($where)->delete();
    }

    /**
     * 修改一条数据
     */

    public function updOne($where, $data)
    {
        return self::where($where)->save($data);
    }

    /**
     * 获取一条数据
     */

    public function selOne($where)
    {
        return self::where($where)->find();
    }
}
