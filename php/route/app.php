<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});

Route::get('hello/:name', 'index/hello');

// 资源路由
Route::resource('school', 'School')->allowCrossDomain();
// 普通路由 =》登录
Route::get('login', 'User/login')->allowCrossDomain();
Route::post('loginApplets', 'User/loginApplets')->allowCrossDomain();
Route::post('appletsUserInfo', 'User/appletsUserInfo')->allowCrossDomain();