-- 创建数据库 0608
create database `0608` charset utf8mb4;


CREATE TABLE `school` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学校ID',
  `name` varchar(30) NOT NULL DEFAULT '未知' COMMENT '学校名',
  `city` varchar(30) NOT NULL DEFAULT '未知' COMMENT '学校城市',
  `num` varchar(30) NOT NULL DEFAULT '0' COMMENT '学校人数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;


CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `name` varchar(30) NOT NULL DEFAULT '未知' COMMENT '用户名',
  `pwd` varchar(32) NOT NULL DEFAULT '未知' COMMENT '用户密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_name` (`name`),
  KEY `u_pwd` (`pwd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `user_info` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `u_openid` varchar(100) NOT NULL DEFAULT '' COMMENT 'openid',
  `u_avatarUrl` varchar(200) NOT NULL DEFAULT '' COMMENT '用户头像',
  `u_city` varchar(100) DEFAULT '' COMMENT '城市',
  `u_country` varchar(100) DEFAULT '' COMMENT '国家',
  `u_gender` varchar(255) DEFAULT '',
  `u_language` varchar(100) DEFAULT '' COMMENT '默认语言',
  `u_nickName` varchar(100) NOT NULL DEFAULT '' COMMENT '用户昵称',
  `u_regtime` int(11) NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_openid` (`u_openid`),
  UNIQUE KEY `u_id` (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;